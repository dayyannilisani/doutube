console.clear()

const cors = require('cors')
const helmet = require('helmet')
const config = require('config')
const express = require('express')
const mongoose = require('mongoose')
const AdminBro = require('admin-bro')
const log = require('debug')('node:')
const AdminBroExpress = require('@admin-bro/express')
const AdminBroMongoose = require('@admin-bro/mongoose')

const winston = require('./util/winston')
const logMiddleware = require('./middleware/log')
const errorMiddleware = require('./middleware/error')
const authMidleware = require('./middleware/auth')
const userRouter = require('./router/user')
const authRouter = require('./router/auth')
const meRouter = require('./router/me')
const videoRouter = require('./router/video')
const categoryRouter = require('./router/category')
const commentRouter = require('./router/comment')
const replyRouter = require('./router/reply')

require('./model/category')
require('./model/comment')
require('./model/feeling')
require('./model/history')
require('./model/reply')
require('./model/subscription')
require('./model/user')
require('./model/video')

AdminBro.registerAdapter(AdminBroMongoose)
const port = config.get('express.port')
const app = express()

const admin = config.get('admin')
const dbConfig = config.get('db')

start()

async function start() {
    //connect to database
    let db
    try {
        db = await mongoose.connect(`mongodb://${dbConfig.address}/${dbConfig.name}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        log('app is connected to database')
    } catch (err) {
        log(err)
        winston.log('error', err)
        process.exit(-1)
    }

    //enable json
    app.use(express.json())

    //adminBro Panel
    {
        const adminBro = new AdminBro({
            databases: [db],
            rootpath: '/admin'
        })
        const adminRouter = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
            authenticate: async (username, password) => {
                if (admin.username == username && admin.password == password)
                    return admin
                return null
            },
            cookieName: 'youtubeClonePanel',
            cookiePassword: config.get("admin.cookiePass"),
        }, null, {
            resave: true,
            saveUninitialized: true,
            logging: false
        })
        app.use(adminBro.options.rootpath, adminRouter)
        log('admin panel is ready on \\admin')
    }

    //security
    app.use(helmet())
    app.use(cors())

    //auth
    app.use(authMidleware)

    //logging
    if (config.get('express.logRequests')) {
        app.use(logMiddleware)
    }

    //test routes
    app.get('/api', (req, res) => {
        res.send("hello world")
    })


    //main Routers
    app.use('/api/user', userRouter)
    app.use('/api/auth', authRouter)
    app.use('/api/me', meRouter)
    app.use('/api/video', videoRouter)
    app.use('/api/category', categoryRouter)
    app.use('/api/comment', commentRouter)
    app.use('/api/reply', replyRouter)

    //showing one simple clip 
    //note: id of the clip should be changed to something that is uploaded
    app.get('/', (req, res) => {
        res.sendFile(__dirname + '/static/index.html')
    })

    //error handling 
    app.use(errorMiddleware)

    //starting express app
    app.listen(port, () => {
        log(`server is listening on ${port}`)
    })
}