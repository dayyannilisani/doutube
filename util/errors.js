const config = require('config')

class MyError extends Error {
    constructor(error, message = "") {
        super()
        this.code = error.code
        this.status = error.status
        this.userMessage = error.userMessage
        if (message != "" && config.get('env') != 'prod')
            this.message = message
        else
            this.message = error.message
    }
}
module.exports.errors = {
    internalServerError: {
        code: 0,
        status: 500,
        message: "internal Server Error",
        userMessage: "خطای داخلی سرور"
    },
    unauthorized: {
        code: 1,
        status: 401,
        message: "you're not allowed",
        userMessage: "شما مجاز به انجام این عملیات نمی باشید"
    },
    invalidToken: {
        code: 2,
        status: 401,
        message: "your token is not valid",
        userMessage: "توکن شما معتبر نمی باشد"
    },
    itemNotFound: {
        code: 3,
        status: 404,
        message: "requested user is not found",
        userMessage: "ایتم مورد نظر پیدا نشد"
    },
    badRequest: {
        code: 4,
        status: 400,
        message: "",
        userMessage: "اطلاعات وارد شده صحیح نمی باشد"
    },
    loginRequired: {
        code: 5,
        status: 401,
        message: "please login",
        userMessage: "لطفا به سیستم وارد شوید"
    },
    invalidUserPass: {
        code: 6,
        status: 400,
        message: "your username or password is incorrect",
        userMessage: "ایمیل یا رمز عبور شما اشتباه می باشد"
    },
    invalidFile: {
        code: 7,
        status: 400,
        message: "",
        userMessage: "فایل مورد نظر مورد قبول نمی باشد"
    },
    noFileToUpload: {
        code: 8,
        status: 400,
        message: "",
        userMessage: "فایلی برای ذخیره پیدا نشد"
    }
}
module.exports.MyError = MyError