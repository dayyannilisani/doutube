const log = require('debug')('node: file:')
const multer = require('multer')
const {
    v4: uuidv4
} = require('uuid');
const {
    errors,
    MyError
} = require('./errors')

const imageStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/images/')
    },
    filename: function (req, file, cb) {
        cb(null, req.tokenBody.id + '.png')
    }
})
const videoStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/videos/')
    },
    filename: function (req, file, cb) {
        cb(null, uuidv4() + '.mp4')
    }
})


const imageUpload = multer({
    storage: imageStorage,
    fileFilter: (req, file, cb) => {
        if (file.fieldname.includes("/jpeg") || file.fieldname.includes("/png") || file.fieldname.includes("/tif"))
            cb(null, true)
        else
            cb(new MyError(errors.invalidFile), false)
    }
})
const videoUpload = multer({
    storage: videoStorage,
    fileFilter: (req, file, cb) => {
        if (file.fieldname.includes("video"))
            cb(null, true)
        else
            cb(new MyError(errors.invalidFile), false)
    }
})


module.exports.imageUpload = imageUpload
module.exports.videoUpload = videoUpload