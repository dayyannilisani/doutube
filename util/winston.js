const {
    createLogger,
    format,
    transports
} = require('winston');

const winston = createLogger({
    level: 'info',
    format: format.simple(),
    transports: [
        new transports.File({
            filename: './log/errors.log',
            level: 'error'
        })
    ]
})

module.exports = winston