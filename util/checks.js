const {
    errors,
    MyError
} = require('./errors')
const log = require('debug')("node: check:")

module.exports.checkIsAdmin = function (req, res, next) {
    log(req.tokenBody)
    if (!req.tokenBody || req.tokenBody.role != "admin") {
        throw new MyError(errors.unauthorized)
    }
    next()
}

module.exports.checkLogin = function (req, res, next) {
    if (!req.tokenBody) {
        throw new MyError(errors.loginRequired)
    }
    next()
}


module.exports.checkIsAdminNotML = function (req) {
    if (!req.tokenBody || req.tokenBody.role != "admin") {
        throw new MyError(errors.unauthorized)
    }
}


module.exports.checkIsNull = function (item) {
    if (!item) {
        throw new MyError(errors.itemNotFound)
    }
}

module.exports.checkOwnerOrAdmin = function (req, item) {
    if (item.userId != req.tokenBody.id && req.tokenBody.role != 'admin')
        throw new MyError(errors.unauthorized)
}