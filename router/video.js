const router = require('express').Router()

const {
    checkLogin
} = require('../util/checks')

const {
    uploadVideo,
    deleteVideo,
    updateVideo,
    getVideoInfo,
    getVideos,
    playVideo
} = require('../controller/video')

const {
    createFeeling
} = require('../controller/feeling')

const {
    videoUpload
} = require('../util/multer')

router
    .route('/')
    .post(checkLogin, videoUpload.single('video'), uploadVideo)


router
    .route('/list/:offset/:limit')
    .get(getVideos)

router
    .route('/one/:id')
    .delete(checkLogin, deleteVideo)
    .put(checkLogin, updateVideo)
    .get(getVideoInfo)

router
    .route('/play/:url')
    .get(playVideo)

router
    .route('/like')
    .post(checkLogin, createFeeling)

module.exports = router