const router = require('express').Router()

const {
    getAllUsers,
    getUser,
    updateUser,
    deleteUser,
    createUser
} = require('../controller/user')

const {
    checkIsAdmin
} = require('../util/checks')

router
    .route('/')
    .get(checkIsAdmin, getAllUsers)
    .post(createUser)

router
    .route('/:id')
    .get(getUser)
    .put(checkIsAdmin, updateUser)
    .delete(checkIsAdmin, deleteUser)

module.exports = router