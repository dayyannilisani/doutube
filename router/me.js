const router = require('express').Router()
const {
    checkLogin
} = require('../util/checks')
const {
    updatePhoto,
    myInfo,
    deleteMe,
    updateMe
} = require('../controller/me')
const {
    getHistory,
    deleteHistory
} = require('../controller/history')

const {
    mySubscribers,
    mySubscribtion,
    removeSubscriber,
    subscribe,
    unSubscribe
} = require('../controller/subscription')

const {
    getMyLikedVideos
} = require('../controller/feeling')

const {
    imageUpload
} = require('../util/multer')

router
    .route('/photo')
    .post(checkLogin, imageUpload.single('photo'), updatePhoto)

router
    .route('/')
    .get(checkLogin, myInfo)
    .put(checkLogin, updateMe)
    .delete(checkLogin, deleteMe)

router
    .route('/history')
    .get(checkLogin, getHistory)

router
    .route('/history/:id')
    .delete(checkLogin, deleteHistory)

router
    .route('/subscribtion')
    .post(checkLogin, subscribe)

router
    .route('/subscribtion/:channelId')
    .delete(checkLogin, unSubscribe)

router
    .route('/subscribtion/:offset/:limit')
    .get(checkLogin, mySubscribtion)

router
    .route('/subscribers/:offset/:limit')
    .get(checkLogin, mySubscribers)

router
    .route('/subscribers/:subscriberId')
    .delete(checkLogin, removeSubscriber)

router
    .router('/likes/:offset/:limit')
    .get(checkLogin, getMyLikedVideos)

module.exports = router