const router = require('express').Router()
const {
    createCategories,
    deleteCategries,
    getCategories,
    updateCategory
} = require('../controller/category')
const {
    checkIsAdmin
} = require('../util/checks')

router
    .route('/')
    .post(checkIsAdmin, createCategories)

router
    .route('/:offset/:limit')
    .get(getCategories)

router
    .route('/:id')
    .delete(checkIsAdmin, deleteCategries)
    .put(checkIsAdmin, updateCategory)

module.exports = router