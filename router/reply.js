const router = require('express').Router()
const {
    deleteReply,
    updateReply,
    createReply,
    getCommentReplies
} = require('../controller/reply')
const {
    checkLogin
} = require('../util/checks')

router
    .route('/')
    .post(checkLogin, createReply)

router
    .route('/:id')
    .delete(checkLogin, deleteReply)
    .put(checkLogin, updateReply)

router
    .route('/:commentId/:offset/:limit')
    .get(getCommentReplies)

module.exports = router