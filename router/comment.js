const router = require('express').Router()
const {
    checkLogin
} = require('../util/checks')
const {
    createComment,
    getUserComments,
    getVideoComments,
    deleteComment,
    updateComment
} = require('../controller/comment')

router
    .route('/')
    .post(checkLogin, createComment)

router
    .route('/user/:offset/:limit')
    .get(checkLogin, getUserComments)

router
    .route('/video/:videoId/:offset/:limit')
    .get(getVideoComments)

router
    .route('/:id')
    .put(checkLogin, updateComment)
    .delete(checkLogin, deleteComment)

module.exports = router