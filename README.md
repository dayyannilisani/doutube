# DueTube Restful Api - YouTubeClone
> Usign nodejs, express, mongodb

## Features

> CRUD (Create, Read, Update And Delete)

- Authentication with JWT
- Pagination
- Admin Panel
- API Security (helmet, noSql inject)
- Video (CRUD)
- Subscribe to a channel
- View liked videos
- Subscriptions
- History (CRUD)
- Upload channel avatar
- Deploy usign Docker and Nginx





I should thank techreagan and his Awesome Code
- https://github.com/techreagan/youtube-clone-nodejs-api/