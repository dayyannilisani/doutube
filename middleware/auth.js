const jwt = require('jsonwebtoken')
const config = require('config')
const log = require('debug')('node: auth:')
const {
    errors,
    MyError
} = require('../util/errors')

module.exports = async function (req, res, next) {
    let token
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1]
    }

    if (!token) {
        return next()
    }

    try {
        const decoded = jwt.verify(token, config.get("jwt.pass"))
        req.tokenBody = {
            id: decoded.id,
            role: decoded.role
        }
        next()
    } catch (err) {
        throw new MyError(errors.invalidToken, err)
    }

}