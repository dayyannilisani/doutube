const winston = require('../util/winston')
const {
    errors,
    MyError
} = require('../util/errors')
const config = require('config')
const log = require('debug')("node: error:")

module.exports = (incomingError, req, res, next) => {

    //write error into log file
    request = ""
    if (JSON.stringify(req.body))
        request = JSON.stringify(req.body)
    logMessage = `\nurl = ${req.url}\nmethod = ${req.method}\nrequest body = {${request}}\nerror = {${incomingError.message}}\nstack = ${incomingError.stack}\n---------------------------`
    winston.log({
        level: 'error',
        message: logMessage
    })

    //create user friendly error
    var outGoingError
    if (incomingError.message.includes("validation failed")) {
        outGoingError = Object.assign({}, errors.badRequest)
        outGoingError.message = ""
        for (item in incomingError.errors) {
            outGoingError.message += incomingError.errors[item].message + '\n'
        }
    } else if (incomingError instanceof MyError) {
        outGoingError = Object.assign({}, incomingError)
    } else {
        outGoingError = Object.assign({}, errors.internalServerError)
        if (config.get('env') != 'prod')
            outGoingError.message = incomingError.message
    }
    const status = outGoingError.status
    delete outGoingError.status
    res.status(status).json(outGoingError)
}