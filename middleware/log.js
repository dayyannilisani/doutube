const log = require("debug")("Node:")

module.exports = function logResponseBody(req, res, next) {
    var oldWrite = res.write,
        oldEnd = res.end;

    var chunks = [];

    res.write = function (chunk) {
        chunks.push(chunk);

        oldWrite.apply(res, arguments);
    };

    res.end = function (chunk) {
        if (chunk) chunks.push(chunk);
        try {
            var body = Buffer.concat(chunks).toString("utf8");
            var request = ""
            if (JSON.stringify(req.body))
                request = JSON.stringify(req.body)
            logMessage = `\nurl = ${req.url}\nmethod = ${req.method}\nrequest body = {${request}}\nresponse body = {${body}}\n---------------------------`
            log(logMessage)
        } catch (err) {}

        oldEnd.apply(res, arguments);
    };

    next();
};