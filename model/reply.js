const mongoose = require('mongoose')

const Schema = mongoose.Schema

const ReplySchema = new Schema({
    text: {
        type: String,
        required: [true, "لطفا یک متن مناسب انتخاب کنید"],
        minlength: [3, 'متن شما باید حداقل سه حرف باشد'],
        maxlength: [1000, "طول متن مورد نظر بیشتر از حد پیش بینی شده می باشد"]
    },
    commentId: {
        type: Schema.ObjectId,
        ref: 'Comment',
        required: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    toJSON: {
        virtuals: true
    },
    toObject: {
        virtuals: true
    },
    timestamps: true
})

ReplySchema.pre('find', function () {
    this.populate({
        path: 'userId',
        select: 'channelName photoUrl',
        sort: '+createdAt'
    })
})


module.exports = mongoose.model('Reply', ReplySchema)