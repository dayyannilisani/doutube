const mongoose = require('mongoose')

const Schema = mongoose.Schema

const FeelingSchema = new Schema({
    type: {
        type: String,
        enum: ['like', 'dislike'],
        required: true
    },
    videoId: {
        type: Schema.ObjectId,
        ref: 'Video',
        required: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Feeling', FeelingSchema)