const mongoose = require('mongoose')

const Schema = mongoose.Schema

const HistorySchema = new Schema({
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    videoId: {
        type: Schema.ObjectId,
        ref: 'Video',
        required: true
    }
})

module.exports = mongoose.model('History', HistorySchema)