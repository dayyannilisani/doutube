const mongoose = require('mongoose')

const Schema = mongoose.Schema

const CommentSchema = new Schema({
    text: {
        type: String,
        required: [true, "لطفا یک متن مناسب انتخاب کنید"],
        minlength: [3, 'متن شما باید حداقل سه حرف باشد'],
        maxlength: [1000, "طول متن مورد نظر بیشتر از حد پیش بینی شده می باشد"]
    },
    videoId: {
        type: Schema.ObjectId,
        ref: 'Video',
        required: true
    },
    userId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    toJSON: {
        virtuals: true
    },
    toObject: {
        virtuals: true
    },
    timestamps: true
})

CommentSchema.virtual('replies', {
    ref: 'Reply',
    localField: '_id',
    foreignField: 'commentId',
    justOne: false,

    options: {
        sort: {
            createdAt: -1
        }
    }
})

module.exports = mongoose.model('Comment', CommentSchema)