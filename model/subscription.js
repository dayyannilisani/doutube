const mongoose = require('mongoose')

const Schema = mongoose.Schema

const SubscribtionSchema = new Schema({
    subscriberId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    },
    channelId: {
        type: Schema.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Subscription', SubscribtionSchema)