const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const Schema = mongoose.Schema

const VideoSchema = new Schema({
    title: {
        type: String,
        unique: [true, 'عنوان مورد نظر باید یکتا باشد'],
        required: [true, "لطفا یک عنوان مناسب انتخاب کنید"],
        minlength: [3, 'عنوان شما باید حداقل سه حرف باشد'],
        maxlength: [100, "طول عنوان مورد نظر بیشتر از حد پیش بینی شده می باشد"]
    },
    description: {
        type: String,
        default: "",
        maxlength: [3000, "طول توضیحات مورد نظر بیشتر از حد پیش بینی شده می باشد"],
    },
    views: {
        type: Number,
        default: 0
    },
    url: {
        type: String,
    },
    categoryId: {
        type: Schema.ObjectId,
        ref: 'Category'
    },
    userId: {
        type: Schema.ObjectId,
        ref: "User",
        required: true
    }
}, {
    toJSON: {
        virtuals: true
    },
    toObject: {
        virtuals: true
    },
    timestamps: true
})

VideoSchema.virtual('dislikes', {
    ref: 'Feeling',
    localField: '_id',
    foreignField: 'videoId',
    justOne: false,
    count: true,
    match: {
        type: 'dislike'
    }
})

VideoSchema.virtual('likes', {
    ref: 'Feeling',
    localField: '_id',
    foreignField: 'videoId',
    justOne: false,
    count: true,
    match: {
        type: 'like'
    }
})

VideoSchema.virtual('comments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'videoId',
    justOne: false,
    count: true
})

VideoSchema.plugin(uniqueValidator, {
    message: `ٰ{VALUE} مورد نظر قبلا استفاده شده است`
})

module.exports = mongoose.model('Video', VideoSchema)