const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const config = require('config')
const uniqueValidator = require('mongoose-unique-validator')
const Schema = mongoose.Schema

const UserSchema = new Schema({
    email: {
        type: String,
        unique: [true, 'ایمیل مورد نظر از استفاده شده می باشد'],
        uniqueCaseInsensitive: [true, 'ایمیل مورد نظر از استفاده شده می باشد'],
        required: [true, 'لطفا یک ایمیل برای خود انتخاب کنید'],
        minlength: [5, 'ایمیل شما باید حداقل پنج حرف باشد'],
        maxlength: [100, "طول ایمیل مورد نظر بیشتر از حد پیش بینی شده می باشد"],
        match: [
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
            'ورودی شما ایمیل درستی نمی باشد'
        ],
    },
    username: {
        type: String,
        unique: [true, 'ایمیل مورد نظر از استفاده شده می باشد'],
        uniqueCaseInsensitive: [true, "نام کاربری مورد نظر استفاده شده است"],
        required: [true, "لطفا یک نام مناسب برای خود انتخاب کنید"],
        minlength: [3, "نام مورد نظر باید حداقل سه حرف باشد"],
        maxlength: [100, "طول اسم مورد نظر بیشتر از حد پیش بینی شده می باشد"],
    },
    password: {
        type: String,
        required: [true, 'لطفا یک  پسورد مناسب انتخاب کنید'],
        minlength: [8, 'رمز شما باید حداقل هشت حرف باشد'],
        select: false
    },
    photo: {
        type: String,
        default: 'no-photo.png'
    },
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    },
    channelName: {
        type: String,
        unique: [true, 'ایمیل مورد نظر از استفاده شده می باشد'],
        uniqueCaseInsensitive: [true, "نام کانال مورد نظر استفاده شده است"],
        required: [true, "لطفا یک نام کانال مناسب برای خود انتخاب کنید"],
    }
}, {
    toJSON: {
        virtuals: true
    },
    toObject: {
        virtuals: true
    },
    timestamps: true
})

UserSchema.index({
    channelName: 'text'
})

UserSchema.virtual('subscribers', {
    ref: 'Subscription',
    localField: '_id',
    foreignField: 'channelId',
    justOne: false,
    count: true,
    match: {
        userId: this._id
    }
})

UserSchema.virtual('videos', {
    ref: 'Video',
    localField: '_id',
    foreignField: 'userId',
    justOne: false,
    count: true,
})

UserSchema.pre('find', function () {
    this.populate({
        path: 'subscribers'
    })
})

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) {
        next()
    }

    const salt = await bcrypt.genSalt(10)
    this.password = await bcrypt.hash(this.password, salt)
})

UserSchema.methods.matchPassword = async function (enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password)
}

UserSchema.methods.getJWTToken = function () {
    return jwt.sign({
        id: this._id,
        role: this.role
    }, config.get('jwt.pass'), {
        expiresIn: config.get("jwt.expiresIn")
    })
}

UserSchema.plugin(uniqueValidator, {
    message: `ٰ{VALUE} مورد نظر قبلا استفاده شده است`
})
module.exports = mongoose.model('User', UserSchema)