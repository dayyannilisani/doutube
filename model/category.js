const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const Schema = mongoose.Schema

const CategorySchema = new Schema({
    title: {
        type: String,
        unique: [true, 'عنوان مورد نظر باید یکتا باشد'],
        required: [true, "لطفا یک عنوان مناسب انتخاب کنید"],
        minlength: [3, 'عنوان شما باید حداقل سه حرف باشد'],
        maxlength: [100, "طول عنوان مورد نظر بیشتر از حد پیش بینی شده می باشد"]
    },
    description: {
        type: String,
        default: "",
        maxlength: [3000, "طول توضیحات مورد نظر بیشتر از حد پیش بینی شده می باشد"],
    }
})
CategorySchema.plugin(uniqueValidator, {
    message: `ٰ{VALUE} مورد نظر قبلا استفاده شده است`
})
module.exports = mongoose.model('Category', CategorySchema)