const History = require('../model/history')
const asyncHandler = require('../util/asyncHandler')
const {
    errors,
    MyError
} = require('../util/errors')
module.exports.getHistory = asyncHandler(async (req, res, next) => {
    const skip = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    const histories = await History.find({
        userId: req.tokenBody.id
    }).skip(skip).limit(limit).populate('videoId')

    res.json(histories)
})

module.exports.deleteHistory = asyncHandler(async (req, res, next) => {
    const history = await History.findById(req.params.id)
    if (history.userId != req.tokenBody.id)
        throw new MyError(errors.unauthorized)

    history.delete()
    res.send()
})