const User = require('../model/user')
const asyncHandler = require('../util/asyncHandler')
const log = require('debug')('node: me:')
const {
    errors,
    MyError
} = require('../util/errors')

module.exports.updatePhoto = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.tokenBody.id)
    if (!user)
        throw new MyError(errors.invalidToken)

    const photo = (req.file && req.file.fieldname == 'photo') ? req.tokenBody.id + '.png' : 'no-photo.png'
    user.photo = photo
    await user.save()
    res.send()
})

module.exports.myInfo = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.tokenBody.id)
    if (!user)
        throw new MyError(errors.invalidToken)
    res.json(user)
})

module.exports.deleteMe = asyncHandler(async (req, res, next) => {
    await User.findByIdAndDelete(req.tokenBody.id)
    res.send()
})

module.exports.updateMe = asyncHandler(async (req, res, next) => {
    delete req.body.password
    const updatedUser = await User.findByIdAndUpdate(req.tokenBody.id, req.body, {
        new: true,
        runValidators: true,
        context: 'query'
    })
    res.json(updatedUser)
})