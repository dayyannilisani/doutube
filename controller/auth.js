const User = require('../model/user')
const asyncHandler = require('../util/asyncHandler')
const {
    MyError,
    errors
} = require('../util/errors')

// @desc    login into user account and receive token
// @route   POST /api/auth/login
// @access  everyone
module.exports.login = asyncHandler(async (req, res, next) => {
    const {
        email,
        password
    } = req.body
    if (!email || !password)
        throw new MyError(errors.badRequest)
    const user = await User.findOne({
        email
    }).select('+password')
    if (!user)
        throw new MyError(errors.invalidUserPass)

    const isPassOk = await user.matchPassword(password)
    if (!isPassOk)
        throw new MyError(errors.invalidUserPass)

    res.json({
        token: user.getJWTToken(),
        user: user
    })
})