const Video = require('../model/video')
const History = require('../model/history')
const log = require('debug')('node: video:')
const asyncHandler = require('../util/asyncHandler')
const fs = require('fs')
const {
    errors,
    MyError
} = require('../util/errors')
const {
    checkOwnerOrAdmin,
    checkIsNull
} = require('../util/checks')

module.exports.uploadVideo = asyncHandler(async (req, res, next) => {
    if (!req.file)
        throw new MyError(errors.noFileToUpload)
    const videoName = req.file.filename
    const newVideo = new Video({
        userId: req.tokenBody.id,
        title: req.body.title,
        description: req.body.description,
        url: videoName
    })
    try {
        await newVideo.save()
    } catch (e) {
        await fs.unlinkSync(`./uploads/videos/${videoName}`)
        throw e
    }
    res.json(newVideo)
})

module.exports.deleteVideo = asyncHandler(async (req, res, next) => {
    const id = req.params.id
    const video = await Video.findById(id)
    checkOwnerOrAdmin(req, video)
    await video.delete()
    res.send()
})

module.exports.updateVideo = asyncHandler(async (req, res, next) => {
    delete req.body.url
    const updatedVideo = await Video.findById(req.params.id)
    checkOwnerOrAdmin(req, updatedVideo)
    checkIsNull(updatedVideo)
    updatedVideo.title = req.body.title
    updatedVideo.description = req.body.description
    updatedVideo.categoryId = req.body.categoryId
    await updatedVideo.save()
    res.json(updatedVideo)
})

module.exports.getVideos = asyncHandler(async (req, res, next) => {
    const skip = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    if (skip != req.params.offset || limit != req.params.limit)
        throw new MyError(errors.badRequest)
    const videos = await Video.find().limit(limit).skip(skip)
    res.json(videos)
})

module.exports.getVideoInfo = asyncHandler(async (req, res, next) => {
    const video = await Video.findOne({
        _id: req.params.id
    }).populate('userId').populate('categoryId')
    checkIsNull(video)
    if (req.tokenBody.id) {
        const newHistory = new History()
        newHistory.userId = req.tokenBody.id
        newHistory.videoId = req.params.id
        await newHistory.save()
    }
    res.json(video)
})

module.exports.playVideo = asyncHandler(async (req, res, next) => {

    const range = req.headers.range;
    if (!range) {
        res.status(400).send("Requires Range header");
    }
    // get video stats (about 61MB)
    const videoPath = './uploads/videos/' + req.params.url
    const videoSize = fs.statSync(videoPath).size;

    // Parse Range
    // Example: "bytes=32324-"
    const CHUNK_SIZE = 10 ** 6; // 1MB
    const start = Number(range.replace(/\D/g, ""));
    const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

    // Create headers
    const contentLength = end - start + 1;
    const headers = {
        "Content-Range": `bytes ${start}-${end}/${videoSize}`,
        "Accept-Ranges": "bytes",
        "Content-Length": contentLength,
        "Content-Type": "video/mp4",
    };

    // HTTP Status 206 for Partial Content
    res.writeHead(206, headers);

    // create video read stream for this particular chunk
    const videoStream = fs.createReadStream(videoPath, {
        start,
        end
    });

    // Stream the video chunk to the client
    videoStream.pipe(res);
})