const Feeling = require('../model/feeling')
const asyncHandler = require('../util/asyncHandler')

module.exports.createFeeling = asyncHandler(async (req, res, next) => {
    const userId = req.tokenBody.id
    const videoId = req.body.videoId
    const type = req.body.type
    const feeling = await Feeling.findOne({
        userId,
        videoId
    })
    if (!feeling) {
        const newFeeling = new Feeling()
        newFeeling.type = type
        newFeeling.videoId = videoId
        newFeeling.userId = userId
        await newFeeling.save()
        return res.send()
    }
    if (feeling.type == type) {
        await feeling.delete()
        return res.send()
    } else {
        feeling.type = type
        await feeling.save()
        return res.send()
    }
})

module.exports.getMyLikedVideos = asyncHandler(async (req, res, next) => {
    const skip = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    const videos = await Feeling.find({
        id: req.tokenBody.id
    }).skip(skip).limit(limit).populate('videoId')
    res.json(videos)
})