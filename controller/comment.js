const Comment = require('../model/comment')
const asyncHandler = require('../util/asyncHandler')
const {
    checkOwnerOrAdmin,
    checkIsNull
} = require('../util/checks')


module.exports.createComment = asyncHandler(async (req, res, next) => {
    req.body.userId = req.tokenBody.id
    const comment = await Comment.create(req.body)
    res.json(comment)
})

module.exports.deleteComment = asyncHandler(async (req, res, next) => {
    const comment = await Comment.findById(req.params.id)
    checkIsNull(comment)
    checkOwnerOrAdmin(req, comment)
    comment.delete()
    res.send()
})

module.exports.updateComment = asyncHandler(async (req, res, next) => {
    const commnet = await Comment.findById(req.params.id)
    checkIsNull(commnet)
    checkOwnerOrAdmin(req, commnet)
    for (item in comment) {
        if (req.body[item])
            commnet[item] = req.body[item]
    }
    comment.save()
    res.json(commnet)
})

module.exports.getVideoComments = asyncHandler(async (req, res, next) => {
    const limit = parseInt(req.params.limit)
    const skip = parseInt(req.params.offset)
    const comments = await Comment.find({
        videoId: req.params.videoId
    }).skip(skip).limit(limit)
    res.json(comments)
})

module.exports.getUserComments = asyncHandler(async (req, res, next) => {
    const limit = parseInt(req.params.limit)
    const skip = parseInt(req.params.offset)
    const comments = await Comment.find({
        userId: req.tokenBody.id
    }).skip(skip).limit(limit)
    res.json(comments)
})