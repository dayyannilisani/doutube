const Category = require('../model/category')
const log = require('debug')('node: category:')
const asyncHandler = require('../util/asyncHandler')


module.exports.createCategories = asyncHandler(async (req, res, next) => {
    const newCategories = await Category.create(req.body)
    res.json(newCategories)
})

module.exports.deleteCategries = asyncHandler(async (req, res, next) => {
    const ids = req.params.ids.split('.')
    await Category.deleteMany({
        _id: {
            $in: ids
        }
    })
    res.send()
})

module.exports.updateCategory = asyncHandler(async (req, res, next) => {
    const updatedCategory = await Category.findByIdAndUpdate(req.params.id, req.body)
    res.json(updatedCategory)
})

module.exports.getCategories = asyncHandler(async (req, res, next) => {
    const limit = parseInt(req.params.limit)
    const skip = parseInt(req.params.offset)
    const categories = await Category.find().skip(skip).limit(limit)
    res.json(categories)
})