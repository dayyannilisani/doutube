const Subscription = require('../model/subscription')
const asyncHandler = require('../util/asyncHandler')

module.exports.subscribe = asyncHandler(async (req, res, next) => {
    const newSub = new Subscription()
    newSub.subscriberId = req.tokenBody.id
    newSub.channelId = req.body.channelId
    await newSub.save()
    res.json(newSub)
})

module.exports.unSubscribe = asyncHandler(async (req, res, next) => {
    const channelId = req.params.channelId
    await Subscription.findOneAndDelete({
        channelId: channelId,
        subscriberId: req.tokenBody.id
    })
    res.send()
})

module.exports.removeSubscriber = asyncHandler(async (req, res, next) => {
    const subscriberId = req.params.subscriberId
    await Subscription.findOneAndDelete({
        channelId: req.tokenBody.id,
        subscriberId: subscriberId
    })
    res.send()
})

module.exports.mySubscribers = asyncHandler(async (req, res, next) => {
    const skip = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    const subs = await Subscription.find({
        channelId: req.tokenBody.id
    }).skip(skip).limit(limit).populate('subscriberId')
    res.json(subs)
})

module.exports.mySubscribtion = asyncHandler(async (req, res, next) => {
    const skip = parseInt(req.params.offset)
    const limit = parseInt(req.params.limit)
    const subs = await Subscription.find({
        subscriberId: req.tokenBody.id
    }).skip(skip).limit(limit).populate('channelId')
    res.json(subs)
})