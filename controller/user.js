const User = require('../model/user')
const log = require('debug')("node: userController")
const asyncHandler = require('../util/asyncHandler')
const {
    checkIsNull,
    checkIsAdminNotML
} = require('../util/checks')

// @desc    create user
// @route   POST /api/user/
// @access  everyone can access it but only admin can create admin
module.exports.createUser = asyncHandler(async (req, res, next) => {
    if (req.body.role && req.body.role == "admin") {
        checkIsAdminNotML(req)
    }
    const user = await User.create(req.body)
    res.send(user)
})

// @desc    get user info
// @route   GET /api/user/:id
// @access  admin
module.exports.getUser = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id).populate({
        path: 'subscribers'
    }).select('-_id')
    checkIsNull(user)
    res.json(user)
})

// @desc    get all user info
// @route   GET /api/user/
// @access  admin
module.exports.getAllUsers = asyncHandler(async (req, res, next) => {
    const users = await User.find()
    res.json(users)
})

// @desc    delete a user
// @route   DELETE /user/:id
// @access  admin
module.exports.deleteUser = asyncHandler(async (req, res, next) => {
    const user = await User.findById(req.params.id)
    checkIsNull(user)
    await User.findByIdAndDelete(req.params.id)
    res.send()
})

// @desc    update a user
// @route   PUT /user/:id
// @access  admin
module.exports.updateUser = asyncHandler(async (req, res, next) => {
    delete req.body.password
    const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
        context: 'query'
    })
    checkIsNull(updatedUser)
    res.json(updatedUser)
})