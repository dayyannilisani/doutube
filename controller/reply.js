const Reply = require('../model/reply')
const asyncHandler = require('../util/asyncHandler')
const {
    checkOwnerOrAdmin,
    checkIsNull
} = require('../util/checks')

module.exports.createReply = asyncHandler(async (req, res, next) => {
    req.body.userId = req.tokenBody.id
    const reply = await Reply.create(req.body)
    res.json(reply)
})

module.exports.updateReply = asyncHandler(async (req, res, next) => {
    const reply = await Reply.findById(req.params.id)
    checkIsNull(reply)
    checkOwnerOrAdmin(req, reply)
    for (item in reply) {
        if (req.body[item])
            reply[item] = req.body[item]
    }
    await reply.save()
    res.json(reply)
})

module.exports.deleteReply = asyncHandler(async (req, res, next) => {
    const reply = await Reply.findById(req.params.id)
    checkIsNull(reply)
    checkOwnerOrAdmin(req, reply)
    await reply.delete()
    res.send()
})

module.exports.getCommentReplies = asyncHandler(async (req, res, next) => {
    const limit = parseInt(req.params.limit)
    const skip = parseInt(req.params.offset)
    const replies = await Reply.find({
        commentId: req.params.commentId
    }).skip(skip).limit(limit)
    res.json(replies)
})